# ![](./plugin/icons/default-48.png) HeaderControl

## Description

mHeaderControl is a Firefox Plugin which provides a bunch of features to manipulates HTTP headers of requests.

Individual features can be turned on and off globally or on a per page basis.

## Features

### Overview

#### Block Cookie SameSite

Removes all cookie headers from requests to resources with the same domain as the site.

#### Block Cookie 3rd-Party

Removes all cookie headers from requests to resources with a different domain as the site.
*(Unfortunately, this feature is currently broken and blocks other cookies as well in certain circumstances.)*

#### Allow CORS

Adds the ```Access-Control-Allow-Origin``` as well as ```Access-Control-Allow-Credentials``` header to all responses.

#### Remove Referer

Removes the referer from all requests.

#### Fake Referer

Removes the referer from all requests and replaces it with a referer which is the same as the requested resource.

#### Prevent Caching

Adds ```Cache-Control```, ```Pragma``` and ```Expires``` header to prevent caching.

#### Disable CSP

Removes ```Content-Security-Policy``` header (and deprecated variants) to disable CSP.

## Screenshots

![](./res/screenshot_1.png)

![](./res/screenshot_2.png)
