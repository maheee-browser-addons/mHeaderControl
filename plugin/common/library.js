if (!window.lib) window.lib = {};


lib.tabUrlStorage = {}; // stores the URLs of tabs in memory

lib.options = [
  {
    id: 'block-cookie-same-site',
    name: 'Block Cookie SameSite'
  },
  {
    id: 'block-cookie-3rd-party',
    name: 'Block Cookie 3rd-Party'
  },
  {
    id: 'allow-cors',
    name: 'Allow CORS'
  },
  {
    id: 'remove-referer',
    name: 'Remove Referer'
  },
  {
    id: 'fake-referer',
    name: 'Fake Referer'
  },
  {
    id: 'prevent-caching',
    name: 'Prevent Caching'
  },
  {
    id: 'disable-csp',
    name: 'Disable CSP'
  },
];

lib.aggregateSettings = function (page, def) {
  var res = {};
  for (let option of lib.options) {
    if (page && page[option.id] == true) {
      res[option.id] = true;
    } else if (page && page[option.id] == false) {
      res[option.id] = false;
    } else {
      res[option.id] = !(!def) && !(!def[option.id]);
    }
  }
  return res;
};

lib.getTabInformation = function (id) {
  if (id < 0) {
    return Promise.resolve(createInfoObject(null));
  }

  function createInfoObject(url) {
    let hostname = lib.getHostnameFromUrl(url);
    return {
      url: url ? new URL(url) : new URL("about:blank"),
      hostname: hostname || url || '_undefined-hostname_'
    };
  }

  return new Promise((resolve, reject) => {
    let storedUrl = lib.tabUrlStorage["" + id];
    if (storedUrl) {
      resolve(createInfoObject(storedUrl));
    } else {
      browser.tabs.get(id).then(tab => {
        resolve(createInfoObject(tab && tab.url));
      });
    }
  });
};

lib.getTabHostname = function (id) {
  return new Promise((resolve, reject) => {
    lib.getTabInformation(id).then((tabInformation) => {
      resolve(tabInformation && tabInformation.hostname);
    });
  });
};

lib.getActiveTabHostname = function () {
  return new Promise((resolve, reject) => {
    browser.tabs.query({active: true}).then(tabs => {
      let hostname = tabs && tabs.length > 0 && lib.getHostnameFromUrl(tabs[0].url);
      resolve(hostname || tabs[0].url || '_undefined-hostname_');
    });
  });
};

lib.getHostnameFromUrl = function (url) {
  if (url) {
    return (new URL(url)).hostname;
  }
  return null;
};

lib.retrieveSettings = function (hostname) {
  return new Promise((resolve, reject) => {
    browser.storage.local.get(['page_' + hostname, 'default']).then(data => {
      resolve({
        page: data['page_' + hostname] || {},
        default: data['default'] || {}
      });
    });
  });
}

lib.storePageSettings = function (hostname, data) {
  let d = {};
  d['page_' + hostname] = data;
  browser.storage.local.set(d);
};

lib.storeDefaultSettings = function (data) {
  let d = {};
  d['default'] = data;
  browser.storage.local.set(d);
};
