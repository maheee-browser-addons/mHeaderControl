if (!window.lib) window.lib = {};


lib.createPopup = function (parentTable, hostname, pageSettings, defaultSettings) {
  for (let option of lib.options) {
    let line = document.createElement('tr');
    let column1 = document.createElement('td');
    let column2 = document.createElement('td');
    let column3 = document.createElement('td');

    column1.textContent = option.name;

    let pageOptionButton = lib.createTristateButton(
        'page_' + option.id,
        pageSettings && pageSettings[option.id],
        (name, newStatus) => {
          pageSettings[name.substring(5)] = newStatus;
          lib.storePageSettings(hostname, pageSettings);
        });
    let defaultOptionButton = lib.createTristateButton(
        'default_' + option.id,
        defaultSettings && defaultSettings[option.id],
        (name, newStatus) => {
          defaultSettings[name.substring(8)] = newStatus;
          lib.storeDefaultSettings(defaultSettings);
        }, true);

    column2.appendChild(pageOptionButton);
    column3.appendChild(defaultOptionButton);

    line.appendChild(column1);
    line.appendChild(column2);
    line.appendChild(column3);

    parentTable.appendChild(line);
  }
};

lib.createIcon = function (name) {
  let icon = document.createElement("i");
  icon.className = "icon icon-" + name;
  return icon;
};

lib.createIconButton = function (iconName, tooltip) {
  let button = document.createElement('div');
  button.className = 'button';
  button.title = tooltip;
  button.appendChild(lib.createIcon(iconName));
  return button;
};

lib.createTristateButton = function (name, status, onChange, twoStateOnly) {
  if (twoStateOnly) {
    status = !(!status);
  }
  let wrapper = document.createElement('div');
  let buttonDef = lib.createIconButton("minus", "Use Default");
  let buttonNo = lib.createIconButton("cancel", "Disable");
  let buttonYes = lib.createIconButton("ok", "Enable");

  wrapper.className = 'btn-group';

  function setClasses(status) {
    buttonDef.className = 'btn btn-default';
    buttonNo.className = 'btn btn-default';
    buttonYes.className = 'btn btn-default';
    if (status == true) {
      buttonYes.className= 'btn btn-default active';
    } else if (status == false) {
      buttonNo.className= 'btn btn-default active';
    } else {
      buttonDef.className= 'btn btn-default active';
    }
  }

  setClasses(status);

  buttonDef.onclick = function () {
    onChange(name, undefined);
    setClasses(undefined);
  };
  buttonNo.onclick = function () {
    onChange(name, false);
    setClasses(false);
  };
  buttonYes.onclick = function () {
    onChange(name, true);
    setClasses(true);
  };

  if (!twoStateOnly) {
    wrapper.appendChild(buttonDef);
  }
  wrapper.appendChild(buttonNo);
  wrapper.appendChild(buttonYes);
  return wrapper;
};
