if (!window.lib) window.lib = {};


lib.initPopup = function () {
  lib.getActiveTabHostname().then((hostname) => {
    lib.retrieveSettings(hostname).then((settings) => {
      let optionTable = document.querySelector('#optionTable');
      lib.createPopup(optionTable, hostname, settings.page, settings.default);
    });
  });
};

lib.initPopup();
