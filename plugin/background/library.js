if (!window.lib) window.lib = {};


lib.removeHeaders = function (headers, removalMap) {
  let i = headers.length;
  while (i--) {
    if (removalMap[headers[i].name.toLowerCase()]) {
      headers.splice(i, 1);
    }
  }
};

lib.requestInfoCache = {};

lib.getRequestInformation = function (request, isFirst, callback) {
  let rId = '' + request.requestId;
  let requestInformation;

  if (isFirst) {
    requestInformation = {}

    lib.getTabInformation(request.tabId).then((tabInformation) => {
      lib.retrieveSettings(tabInformation.hostname).then((settings) => {
        requestInformation.tabHostname = tabInformation.hostname;
        requestInformation.tabUrl = tabInformation.url;
        requestInformation.settings = lib.aggregateSettings(settings.page, settings.default);
        requestInformation.requestUrl = new URL(request.url);
        requestInformation.requestHostname = lib.getHostnameFromUrl(request.url);
        requestInformation.isRequest = true;
        requestInformation.isResponse = false;
        requestInformation.cb = callback(requestInformation);
        lib.requestInfoCache[rId] = requestInformation;
      });
    });
  } else {
    requestInformation = lib.requestInfoCache[rId];
    requestInformation.isRequest = false;
    requestInformation.isResponse = true;

    callback(requestInformation);
  }

  if (!isFirst) {
    delete lib.requestInfoCache[rId];
  }
}
