if (!window.lib) window.lib = {};


lib.modifyHeaders = function (headers, requestInformation, request) {
  function isSameHostname(stored, request) {
    return (stored === request) || ("www." + stored === request);
  }
  let doRemoveCookies = requestInformation.settings['block-cookie-same-site']
      && isSameHostname(requestInformation.tabHostname, requestInformation.requestHostname);
  doRemoveCookies = doRemoveCookies
      || requestInformation.settings['block-cookie-3rd-party']
      && !isSameHostname(requestInformation.tabHostname, requestInformation.requestHostname);
  let doAllowCors = requestInformation.settings['allow-cors'];
  let doRemoveReferer = requestInformation.settings['remove-referer'];
  let doFakeReferer = requestInformation.settings['fake-referer'];
  let doPreventCaching = requestInformation.settings['prevent-caching'];
  let doDisableCsp = requestInformation.settings['disable-csp'];

  let removeHeaderMap = {};

  if (doRemoveCookies) {
    removeHeaderMap['set-cookie'] = true;
    removeHeaderMap['set-cookie2'] = true;
    removeHeaderMap['cookie'] = true;
  }
  if (doAllowCors) {
    removeHeaderMap['access-control-allow-origin'] = true;
    removeHeaderMap['access-control-allow-credentials'] = true;
    removeHeaderMap['x-frame-options'] = true;
  }
  if (doRemoveReferer || doFakeReferer) {
    removeHeaderMap['referer'] = true;
    removeHeaderMap['referrer'] = true;
  }
  if (doPreventCaching) {
    removeHeaderMap['cache-control'] = true;
    removeHeaderMap['pragma'] = true;
    removeHeaderMap['expires'] = true;
  }
  if (doDisableCsp) {
    removeHeaderMap['content-security-policy'] = true;
    removeHeaderMap['x-content-security-policy'] = true;
    removeHeaderMap['x-webkit-csp'] = true;
  }

  lib.removeHeaders(headers, removeHeaderMap);

  function addHeader(name, value) {
    headers.push({ name: name, value: value });
  }

  if (doAllowCors && requestInformation.isResponse) {
    if (requestInformation.tabUrl) {
      addHeader('Access-Control-Allow-Origin', requestInformation.tabUrl.origin);
    } else {
      addHeader('Access-Control-Allow-Origin', '*');
    }
    addHeader('Access-Control-Allow-Credentials', 'true');
  }
  if (doFakeReferer && requestInformation.isRequest) {
    addHeader('Referer', request.url);
  }
  if (doPreventCaching && requestInformation.isRequest) {
    addHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
    addHeader('Pragma', 'no-cache');
    addHeader('Expires', '0');
  }
};

lib.navigationHandler = function (details) {
  if (details.frameId == 0 && details.url) {
    lib.tabUrlStorage["" + details.tabId] = details.url;
  }
};

browser.webNavigation.onCreatedNavigationTarget.addListener(lib.navigationHandler);
browser.webNavigation.onBeforeNavigate.addListener(lib.navigationHandler);
browser.webNavigation.onHistoryStateUpdated.addListener(lib.navigationHandler);
browser.webNavigation.onCommitted.addListener(lib.navigationHandler);

browser.webRequest.onBeforeSendHeaders.addListener(function (request) {
  if (!request || !request.requestHeaders) {
    return {};
  }
  return new Promise((resolve, reject) => {
    lib.getRequestInformation(request, true, (requestInformation) => {

      lib.modifyHeaders(request.requestHeaders, requestInformation, request);

      resolve({
        requestHeaders: request.requestHeaders
      });
      return {};
    });
  });
}, {urls: ['<all_urls>']}, ['blocking' ,'requestHeaders']);

browser.webRequest.onHeadersReceived.addListener(function (request) {
  if (!request || !request.responseHeaders || request.statusCode == 304) {
    return {};
  }
  return new Promise((resolve, reject) => {
    lib.getRequestInformation(request, false, (requestInformation) => {

      lib.modifyHeaders(request.responseHeaders, requestInformation, request);

      resolve({
        responseHeaders: request.responseHeaders
      });
    });
  });
}, {urls: ['<all_urls>']}, ['blocking' ,'responseHeaders']);
